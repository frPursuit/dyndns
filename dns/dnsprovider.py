from typing import List

from pursuitlib import errors

from dns.dnsrecord import DnsRecord


class DnsProvider:
    def __init__(self, zone):
        self.zone = zone

    def get_records(self) -> List[DnsRecord]:
        raise errors.not_implemented(self, self.get_records)

    def create_record(self, record: DnsRecord):
        raise errors.not_implemented(self, self.create_record)

    def delete_record(self, record: DnsRecord):
        raise errors.not_implemented(self, self.delete_record)

    def get_records_for(self, name: str) -> List[DnsRecord]:
        records = self.get_records()
        records_for_name = []

        for record in records:
            if record.name == name:
                records_for_name.append(record)
        return records_for_name
