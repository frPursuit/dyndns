from typing import List

import ovh

from dns.dnsprovider import DnsProvider
from dns.dnsrecord import DnsRecord


class OvhDnsProvider(DnsProvider):
    PROVIDER_NAME = "ovh"

    def __init__(self, endpoint: str, application_key: str, application_secret: str, consumer_key: str, zone: str):
        super().__init__(zone)
        self.client = ovh.Client(
            endpoint=endpoint,
            application_key=application_key,
            application_secret=application_secret,
            consumer_key=consumer_key,
        )

    def get_records(self) -> List[DnsRecord]:
        results = self.client.get(f"/domain/zone/{self.zone}/record", fieldType=DnsRecord.TYPE_A)
        results += self.client.get(f"/domain/zone/{self.zone}/record", fieldType=DnsRecord.TYPE_AAAA)

        records = []
        for result in results:
            records.append(self.get_record_info(result))
        return records

    def create_record(self, record: DnsRecord):
        result = self.client.post(f"/domain/zone/{self.zone}/record", fieldType=record.record_type, subDomain=record.name, target=record.target)
        record.id = str(result["id"])

    def delete_record(self, record: DnsRecord):
        self.client.delete(f"/domain/zone/{self.zone}/record/{record.id}")

    def get_record_info(self, record_id: str) -> DnsRecord:
        result = self.client.get(f"/domain/zone/{self.zone}/record/{record_id}")
        record = DnsRecord(result["subDomain"], result["fieldType"], result["target"])
        record.set_id(record_id)
        return record
