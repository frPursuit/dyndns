from typing import Optional


class DnsRecord:
    TYPE_A = "A"
    TYPE_AAAA = "AAAA"
    TYPE_CNAME = "CNAME"

    def __init__(self, name: str, record_type: str, target: str):
        self.id = None
        self.name = name
        self.record_type = record_type
        self.target = target

    def set_id(self, record_id: Optional[str]):
        self.id = record_id

    def get_id(self) -> Optional[str]:
        return self.id

    def get_name(self) -> str:
        return self.name

    def get_record_type(self) -> str:
        return self.record_type

    def get_target(self) -> str:
        return self.target

    def __eq__(self, other) -> bool:
        if not isinstance(other, DnsRecord):
            return False
        return self.name == other.name and self.record_type == other.record_type and self.target == other.target

    def __str__(self) -> str:
        return f"{self.name} IN {self.record_type} {self.target}"
