import subprocess
from typing import List, Optional, Tuple

from pursuitlib.utils import get_env

from dns.dnsprovider import DnsProvider
from dns.dnsrecord import DnsRecord
from dns.ovhdnsprovider import OvhDnsProvider
from settings import DNS_PROVIDER, DNS_ZONE, DNS_DOMAINS_V4, DNS_DOMAINS_V6

__version__ = "0.2.0"
PUBLIC_ADDRESS_SCANNER = "https://ip.me"


def main():
    provider = create_dns_provider()
    existing = provider.get_records()
    ipv4, ipv6 = get_public_addresses()

    if ipv4 is not None:
        update_records(provider, existing, DnsRecord.TYPE_A, DNS_DOMAINS_V4, ipv4)
    if ipv6 is not None:
        update_records(provider, existing, DnsRecord.TYPE_AAAA, DNS_DOMAINS_V6, ipv6)


def create_dns_provider() -> DnsProvider:
    if DNS_PROVIDER == "ovh":
        return OvhDnsProvider(
            get_env("DNS_OVH_ENDPOINT"),
            get_env("DNS_OVH_APP_KEY"),
            get_env("DNS_OVH_APP_SECRET"),
            get_env("DNS_OVH_CONSUMER_KEY"),
            DNS_ZONE
        )
    raise ValueError(f"Invalid DNS provider: {DNS_PROVIDER}")


def update_records(provider: DnsProvider, existing: List[DnsRecord], record_type: str,
                   domains: List[str], address: str):
    target_names = [domain.strip() for domain in domains]
    target_records = []
    for name in target_names:
        target_records.append(DnsRecord(name, record_type, address))

    for record in existing:
        # Ignore non-target names
        if record.get_name() not in target_names:
            continue
        # Ignore non-managed records
        if record.get_record_type() != record_type:
            continue

        if record in target_records:
            target_records.remove(record)
        else:
            print(f"Deleting record: {record}")
            provider.delete_record(record)

    for record in target_records:
        print(f"Creating record: {record}")
        provider.create_record(record)


def get_public_addresses() -> Tuple[Optional[str], Optional[str]]:
    try:
        ipv4 = _get("curl", "-4", PUBLIC_ADDRESS_SCANNER)
    except ChildProcessError:
        # IPv4 is not supported
        ipv4 = None

    try:
        ipv6 = _get("curl", "-6", PUBLIC_ADDRESS_SCANNER)
    except ChildProcessError:
        # IPv6 is not supported
        ipv6 = None

    return ipv4, ipv6


def _get(*command: str) -> str:
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    if result.returncode != 0:
        raise ChildProcessError(f"Unable to perform HTTP request: error code {result.returncode}")
    return result.stdout.decode("utf8").replace("\n", "")


if __name__ == "__main__":
    main()
