import os

from pursuitlib.utils import get_env

DNS_PROVIDER = get_env("DNS_PROVIDER")
DNS_ZONE = get_env("DNS_ZONE")

DNS_DOMAINS_V4 = get_env("DNS_DOMAINS_V4").split(',') if "DNS_DOMAINS_V4" in os.environ else []
DNS_DOMAINS_V6 = get_env("DNS_DOMAINS_V6").split(',') if "DNS_DOMAINS_V6" in os.environ else []
