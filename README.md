# DynDNS

DynDNS is a tool that can be used to dynamically change the DNS records that point to a machine connected to the Internet.

It uses [ip.me](https://ip.me) to detect to machine's IP, and the DNS provider's API to change the records when necessary.

## Usage

This tool must first be configured using environment variables. To do so, create a `.env` file from `.env.example`.

Then, it must be periodically called.

## Supported DNS providers

Currently, the only supported DNS provider is `ovh`.
